var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';

app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/techucperalta/collections/"
var apikeyMLab = "apiKey=8Ep_LZhxVfn86vAhi46RNGyFFRBFbgqQ"

//Get Simple
app.get(URLbase + 'users',
  function(req, res) {
    var query = 'f={"_id":0}&';
    clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + apikeyMLab);
    clienteMlab.get('',
      function(err, resM, body) {
        if (err) {
            res.status(500).send('Ocurrio un error en la conexión');
        }
        else{
          if (body.length > 0)
            res.send(body);
          else {
            res.status(404).send('Usuario no encontrado');
          }
        }
      })
  });

//Get por Id
// app.get(URLbase + 'users/:id',
//   function(req, res) {
//     var id = req.params.id
//     var query = 'q={"id":' + id + '}&f={"nombre":1, "apellidos":1, "_id":0}'
//     clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab)
//     clienteMlab.get('', function(err, resM, body) {
//       if (!err) {
//         if (body.length > 0)
//           res.send(body[0])
//           //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
//         else {
//           res.status(404).send('Usuario no encontrado')
//         }
//       }
//     })
// });

//PUT of user
app.put(URLbase + 'users/:id',
  function(req, res) {
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('user?'+ queryStringID + apikeyMLab ,
      function(error, respuestaMLab , body) {
        console.log(body);
       var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
       clienteMlab.put(baseMLabURL + 'user?q={"userID": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
        function(error, respuestaMLab, body) {
          console.log("body:"+ JSON.stringify(body));
         res.send(body);
        });
   });
  });

// Petición PUT con id de mLab (_id.$oid)
app.put(URLbase + 'usersmLab/:id',
  function (req, res) {
    var id = req.params.id;
    let userBody = req.body;
    var queryString = 'q={"userID":' + id + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('user?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body){
        let response = body[0];
        console.log(body);

        //Actualizo los campos del usuario
        let updatedUser = {
          "userID" : response.userID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };//Otra forma simplificada (para muchas propiedades)
        // var updatedUser = {};
        // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
        // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);

        //Llamo al put de mlab.
        httpClient.put('user/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario actualizado correctamente."
                }
                res.status(404);
              }
            }
            res.send(response);
          });
      });
  });

//DELETE user with id
app.delete(URLbase + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        console.log(body);
        var respuesta = body[0];
        console.log("body delete:"+ JSON.stringify(respuesta));
        httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });

//Get por id
app.get(URLbase + "users/:id",
  function(req, res) {
    var id = req.params.id;
    var query = 'q={"userID":' + id + '}&f={"first_name":1, "last_name":1, "_id":0}';
    clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    console.log(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    clienteMlab.get('',
      function(err, resM, body) {
        if (!err) {
          if (body.length > 0){
            res.send(body[0]);
          //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
          }
          else {
            res.status(404).send('Usuario no encontrado');
          }
        }
    });
  });

// Login - body
app.post(URLbase + "login",
  function(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var query = 'q={"email":"' + email + '","password":"' + password + '"}';
    clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){ // Login ok
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user");
          console.log(body);
          var cambio = '{"$set":{"logged":true}}';
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio),
            function(errP, resP, bodyP) {
              res.send({"login":"ok", "userID":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})
            });
        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    });
  });

// Login - headers
app.post(URLbase + "login",
  function(req, res) {
    var email = req.headers.email;
    var password = req.headers.password;
    var query = 'q={"email":"' + email + '","password":"' + password + '"}';
    clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length == 1){ // Login ok
          clienteMlab = requestJSON.createClient(baseMLabURL + "/user");
          console.log(body);
          var cambio = '{"$set":{"logged":true}}';
          clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio),
            function(errP, resP, bodyP) {
              res.send({"login":"ok", "userID":body[0].userID, "nombre":body[0].first_name, "apellidos":body[0].last_name})
            });
        }
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    });
  });

  //Logout - body
  app.post(URLbase + "logout",
    function(req, res) {
      var email = req.body.email;
      var query = 'q={"email":"' + email + '", "logged":true}';
      console.log(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
      clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
      clienteMlab.get('',
        function(err, resM, body) {
          console.log(err);
          console.log(body);
          if (!err) {
            if (body.length == 1) // Estaba logado
            {
              clienteMlab = requestJSON.createClient(baseMLabURL + "user");
              var cambio = '{"$set":{"logged":false}}';
              clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio),
                function(errP, resP, bodyP) {
                  res.send({"logout":"ok", "userID":body[0].userID})
                });
            }
            else {
              res.status(200).send('Usuario no logado previamente')
            }
          }
        });
  });

//Logout - headers
app.post(URLbase + "logout",
  function(req, res) {
    var id = req.headers.id;
    var query = 'q={"userID":' + id + ', "logged":true}';
    console.log(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    clienteMlab = requestJSON.createClient(baseMLabURL + "user?" + query + "&l=1&" + apikeyMLab);
    clienteMlab.get('',
      function(err, resM, body) {
        if (!err) {
          if (body.length == 1) // Estaba logado
          {
            clienteMlab = requestJSON.createClient(baseMLabURL + "user");
            var cambio = '{"$set":{"logged":false}}';
            clienteMlab.put('?q={"userID": ' + body[0].userID + '}&' + apikeyMLab, JSON.parse(cambio),
              function(errP, resP, bodyP) {
                res.send({"logout":"ok", "userID":body[0].userID})
              });
          }
          else {
            res.status(200).send('Usuario no logado previamente')
          }
        }
      });
});

// app.get('/', function (req, res) {
//   res.send('Hola Lima Peru!');
// });

//app.listen(3000);
app.listen(3000, function () {
  console.log('Node escucha en puerto 3000!');
});
