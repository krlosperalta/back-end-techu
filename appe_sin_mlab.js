var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apiperu/v1/';

app.use(bodyParser.json());
//GET users
app.get(URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/users');
    res.send(userFile);
    //res.status(200).send({"msg": "respuesta correcta"});
  });

//GET users por ID
app.get(URLbase + 'users/:id',
  function(req, res){
    console.log('GET /apiperu/v1/users/:id');
    console.log(req.params);
    console.log(req.params.id);
    let pos = req.params.id;
    let response = (userFile[pos-1]==undefined)?{"msg": "No existe"} : userFile[pos-1];
    res.send(response);
  });

//GET con query string
app.get(URLbase + 'users',
  function(req, res){
    console.log(req.query);
    console.log(req.query.id);
    let pos = req.query.id;
    let response = (userFile[pos-1]==undefined)?{"msg": "No existe"} : userFile[pos-1];
    res.send(response);
  });

//GET con query string monstruo
app.get(URLbase + 'users/:id/:id2',
  function(req, res){
    console.log(req.query);
    console.log(req.params);
  });


//POST users
app.post(URLbase + 'users',
  function(req, res){
    console.log('POST /apiperu/v1/users');
    let newID = userFile.length + 1;
    let newUser = {
      "userID" : newID,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    }
    userFile.push(newUser);
    console.log(userFile);
    res.status(201);
    res.send({"msg":"Usuario añadido correctamente", newUser})
  });

//PUT user
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log('PUT /apiperu/v1/users/:id');
    console.log(req.params.id);
    let pos = req.params.id;
    let updatedUser = {
      "userID" : pos,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : req.body.password
    }
    userFile[pos-1] = updatedUser;
    res.send({"msg":"Usuario modificado correctamente", updatedUser});
  });

//DELETE user
app.delete(URLbase + 'users/:id',
  function(req, res){
    console.log('DELETE /apiperu/v1/users/:id');
    console.log(req.params.id);
    let pos = req.params.id;
    let idUs = userFile[pos-1]["userID"];
    let response = (pos!=idUs)?{"msg": "No existe"} : userFile.splice(pos-1, 1);
    res.send(response);
  });


// app.get('/', function (req, res) {
//   res.send('Hola Lima Peru!');
// });

//app.listen(3000);
app.listen(3000, function () {
  console.log('Node escucha en puerto 3000!');
});
